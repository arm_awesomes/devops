---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# State Diagram

<!-- slide -->
# State and Arrow

1. **`[*]`** 定义开始状态或结束状态（取决于箭头所在位置）
1. **`<state_id>`** 或 **`state "state name" as <state_id>`** 定义状态
1. **`<state_id> : <state description>`** 定义状态的描述

<!-- slide -->

@import "./00_Diagram/demo_state.puml"

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
