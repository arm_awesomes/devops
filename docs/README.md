# DevOps docs

## docs编写环境

1. **编辑器VSCode：** <https://code.visualstudio.com/>
1. **文档语言Markdown：**
    1. **Markdown-Preview-Enhanced官网：** <https://shd101wyy.github.io/markdown-preview-enhanced/#/>
    1. **VSCode Markdown-Preview-Enhanced插件：** <https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced>
1. **绘图语言：**
    1. **PlantUML：**
        1. **PlantUML官网：** <https://plantuml.com/>
        1. **VSCode PlantUML插件：** <https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml>
    1. **Graphviz：**
        1. **Graphviz官网：** <https://www.graphviz.org/>
        1. **VSCode Graphviz插件：** <https://marketplace.visualstudio.com/items?itemName=EFanZh.graphviz-preview>

:warning: :warning: :warning: 在VSCode中使用PlantUML和Graphviz需先安装Graphviz应用程序（[各平台的Graphviz下载地址](http://graphviz.org/download/)）并设置Path:warning: :warning: :warning:

## docs阅读方法

### 方式一：使用markdown阅读工具阅读

可以通过使用`markdown`阅读工具进行阅读，因为使用了[Markdown Preview Enhanced](https://shd101wyy.github.io/markdown-preview-enhanced/#/)（简称“`MPE`”）的一些扩展语法，因此建议使用该插件进行预览。

### 方式二：使用已预导出的html文件

使用`Chrome`或`Firefox`打开（**`IE`无法正常打开**），在`Windows`平台下，大多数情况`Firefox`阅读效果更佳（`Firefox`下`emoji`能完整显示，`Chrome`在有些机器上不能完整显示，具体原因未细究）

:warning:由于自定义了`MPE`的`CSS`，因此，您本地使用`MPE`预览的结果可能与已导出的`html`文件的显示不一样。

### 方式三：markdown导出成PDF后阅读

也可使用`markdown`工具导出成`PDF`进行阅读，由于`PDF`文件较大且为二进制文件，本仓库不包含`PDF`格式的文件。
