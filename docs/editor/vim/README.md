# Vim

## 插件管理器Plugin Managers

当没有插件管理器时，Vim 用户必须手动下载 tarball 包形式的插件，并将它们解压到 ~/.vim  目录中。在少量插件的时候可以。但当他们安装更多的插件时，就会变得一团糟。所有插件文件分散在单个目录中，用户无法找到哪个文件属于哪个插件。此外，他们无法找到他们应该删除哪个文件来卸载插件。这时 Vim 插件管理器就可以派上用场。插件管理器将安装插件的文件保存在单独的目录中，因此管理所有插件变得非常容易。

1. [vim-plug](https://github.com/junegunn/vim-plug)：

## 常用插件Commonly UsedPlugins

1. [NERDTree](https://github.com/preservim/nerdtree)
1. [auto-pairs]()
1. [vim-airline]()
