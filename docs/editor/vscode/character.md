# 字符相关

## 相关插件

1. [Unicode Code Point](https://marketplace.visualstudio.com/items?itemName=medo64.code-point)
1. [Gremlins tracker for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=nhoizey.gremlins)
1. [Highlight Bad Chars](https://marketplace.visualstudio.com/items?itemName=wengerk.highlight-bad-chars)

## Bibliographies

1. [可惡的邪惡隱藏字元，現身吧](https://www.jazz321254.com/evil-hidden-unicode/)
