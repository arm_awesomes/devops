# Extension插件扩展

|插件名称|插件作用|
|--|--|
Auto Close Tag|自动添加HTML / XML关闭标签1.16已经内置 HTML 的自动闭合标签功能，但是此插件能可配置的开启 XML, PHP, Vue, JavaScript, TypeScript, JSX, TSX 等语言的自动闭合标签功能
Auto Import|TypeScript 、TSX 的自动解析和查找可导入的代码
Auto Rename Tag|自动重命名HTML / XML对应的标签
Beautify|美化 javascript, JSON, CSS, Sass, 和 HTML
Beautify css/sass/scss/less|美化 css, sass and less
Better Comments|更人性化的注释
Bookmarks|收藏某一行为书签，并且能快速跳转到某个书签位置
Bracket Pair Colorizer|为括号制定不同的颜色
Code Runner|运行代码片段或文件
Debugger for Chrome|在Chrome浏览器或其他支持Chrome调试器协议的目标中调试JavaScript代码
Document This|自动生成JSDoc，适用于JavaScript 和 TypeScript
EditorConfig for VS Code|EditorConfig 支持
Emoji|从命令面板插入表情符号
ESLint|ESLint 支持
filesize|在状态栏显示当前文件大小
Git History (git log)|查看Git日志、文件或行历史记录
Git Lens|代码提交信息
gitignore|.gitignore 支持
HTML CSS Support|html文件的CSS支持
HTML Snippets|HTML标签片段（包括HTML5）
HTMLHint|HTML静态代码分析工具
Indenticator|突出显示当前缩进的辅助线
IntelliJ IDEA Keybindings|用惯了IntelliJ全家桶，快捷键改不过来
IntelliSense for CSS class names|根据工作区中的CSS文件为HTML class属性提供CSS类名称提示
markdownlint|markdown 语法检查高亮
Material Icon Theme|Material Design Icons
minify|压缩JavaScript、CSS、HTML
npm|npm 支持
npm Intellisense|自动完成NPM模块导入语句
open-in-browser|在浏览器中预览HTML文件
Output Colorizer|输入的日志高亮显示
Path Intellisense|自动完成文件名
Python|Python 支持
Settings Sync|同步设置、片段、主题、文件图标，启动，Keybindings
TS/JS postfix completion|JavaScript、TypeScript 智能提示
TSLint|TypeScript 语言 TSLint 支持
Vetur|支持 Vue 最好的插件
vscode-fileheader|插入头部注释，并自动更新时间
