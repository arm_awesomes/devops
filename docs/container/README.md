# Container

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [What is Container](#what-is-container)
    1. [Function of Container](#function-of-container)
    2. [Characters of Container](#characters-of-container)
2. [History of Container](#history-of-container)
3. [VM vs Container](#vm-vs-container)
4. [Docker and Kubernetes](#docker-and-kubernetes)
5. [Docker with VM](#docker-with-vm)
6. [Futhermore](#futhermore)

<!-- /code_chunk_output -->

## What is Container

### Function of Container

1. application packaging && publishing tool
1. application deploy && runtime tool

### Characters of Container

1. light-weight resource isolation and control
1. `OS` level resource isolation and control

## History of Container

1. 1979: `UNIX V7 chroot`
1. 2000: `FreeBSD Jails`
1. 2001: `Linux VServer`
1. 2004: `Solaris Containers`
1. 2005: `Linux Open VZ(Open Virtuzzo)`
1. 2006: `Linux Process Containers(Linux Control Groups/Cgroups)`, by `Google`
1. 2008: `Linux Containers/LXC`, combined with `Linux Cgroups` and `Linux Namespace`
1. 2011: `CloudFoundry Warden`
1. 2013: `Google LMCTFY`, stopped in 2015
1. 2013: `Docker`
1. 2014: `CoreOS Rocket`
1. 2016: `Windows Containers`, support `Docker` with `OS kernel`

>40 年回顾，一文读懂容器发展史： <https://www.infoq.cn/article/SS6SItkLGoLExQP4uMr5>

## VM vs Container

![virtual-machines_versus_containers](./assets_image/virtual-machines_versus_containers.png)

>[Virtual Machine VS Container](https://medium.com/@deshanigeethika/docker-tutorial-a6aa5b41e3ff)

||VM|Container|
|--|--|--|
|isolation|physical resource|application|
|abstract|machine|process and sub-process|
|sharing|host hardware|host(Linux) hardware, kernel, libraries|

1. Containers virtualize the Operating System and make them more portable
1. Virtual Machines, by contrast, virtualize the hardware.
1. Containers are an abstraction at the application layer that packages code and dependencies together.
1. VMs are an abstraction of physical hardware turning one server into many.

容器没有自己的内核，也没有进行硬件虚拟化，因此，`container`比`VM`要轻便

每个容器有自己的文件系统，因此，`container`之间能进行必要的隔离

## Docker and Kubernetes

像`TCP/IP`定义了`Internet`协议栈、`Unix`定义了`modern OS`一样，`Docker`事实上定义了`application packaging & publishing`、`Kubernetes`事实上定义了`application operating life cycle`

## Docker with VM

![docker-containers-are-not-lightweight-virtual-machines](./assets_image/docker-containers-are-not-lightweight-virtual-machines.png)

>[Kubernetes vs Docker - What Is the Difference?](https://www.nakivo.com/blog/docker-vs-kubernetes/)

![container-based-cloud-vs-VM-based-cloud](./assets_image/container-based-cloud-vs-VM-based-cloud.png)

>[A hybrid genetic programming hyper-heuristic approach for online two-level resource allocation in container-based clouds](https://meiyi1986.github.io/publication/tan-2019-hybrid/tan-2019-hybrid.pdf)

## Futhermore

1. What is a Container?-A standardized unit of software: <https://www.docker.com/resources/what-container>
1. Containers are not VMs: <https://www.docker.com/blog/containers-are-not-vms/>
1. Are Containers Replacing Virtual Machines?: <https://www.docker.com/blog/containers-replacing-virtual-machines/>
1. 40 年回顾，一文读懂容器发展史： <https://www.infoq.cn/article/SS6SItkLGoLExQP4uMr5>
1. 关于容器、微服务、docker 的十大问题： <https://www.infoq.cn/article/iqrVKSrBzZzo64h3umAW>
