# Docker Host

## Host in Docker Desktop for Mac

>1. [Docker for Mac Commands for Getting Into The Local Docker VM](https://www.bretfisher.com/docker-for-mac-commands-for-getting-into-local-docker-vm/)
>1. [macos中docker的存储路径问题](https://blog.donghy.cn/20200215133.html)

与`Docker`在`Linux`系统不同，`Docker`在`MacOS`中的宿主机并不是`MacOS`本身，而是`Docker Desktop for Mac`启动的`VM`(`Alpine Linux`)，因此，`Docker Desktop for Mac`的`volume`存储在`VM`（而不是`MacOS`）中的`/var/lib/docker`中。

这台`VM`的全部内容存储在一个`qcow2`，该文件也存储了`Docker Desktop for Mac`的全部内容（包括`VM`，`image`，`container`，`volume`等），该文件默认存储于`~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/Docker.qcow2`（在`v2.3`中可通过`[Preferences] - > [Resources] - > [Disk image location]`修改存储路径）。

可通过以下几种方法进入该`VM`。

```bash {.line-numbers}
###### method 01 ######
# connect to tty on Docker for Mac VM
screen ~/Library/Containers/com.docker.docker/Data/vms/0/tty

# disconnect that session but leave it open in background
Ctrl-a d

# list that session that's still running in background
screen -ls

# reconnect to that session (don't open a new one, 
# that won't work and 2nd tty will give you garbled screen)
screen -r

# kill this session (window) and exit
Ctrl-a k

###### method 02 ######
# other option to connect w/o screen
docker run -it --privileged --pid=host debian nsenter -t 1 -m -u -n -i sh
# Phil Estes says this does:
# it’s running a container (using the debian image..
# nothing special about it other than it apparently has `nsenter` installed), 
# with pid=host (so you are in the process space of the mini VM running Docker4Mac),
# and then nsenter says “whatever is pid 1,
# use that as context, and enter all the namespaces of that, and run a shell there"

###### method 03 ######

# from Justin Cormack
# Personally I mostly use screen, but then I also use
# too. That's my minimal nsenter image
docker run --privileged --pid=host justincormack/nsenter1
```

>:information_source: original: [joacim-boive/screen-docker-for-mac.sh](https://gist.github.com/joacim-boive/569b66c3be673a3f3e802939fe8edd56)
>
>:warning: 原文中第一种方法`screen ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/tty`可能在新版本中的`Docker Desktop for Mac`已变更成`screen ~/Library/Containers/com.docker.docker/Data/vms/0/tty`

`MacOS`与`VM`可设置共享目录（在`v2.3`中可通过`[Preferences] - > [Resources] - > [File Sharing]`设置）。

## Futhermore
