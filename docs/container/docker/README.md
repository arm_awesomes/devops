# Docker

## What is Docker?

1. 2010 - 2011 : `Docker Inc.` was founded by Solomon Hykes and Sebastien Pahl during the Y Combinator Summer 2010 startup incubator group and launched in 2011
1. 2013/03/15: `Docker` debuted to the public in Santa Clara at `PyCon`(2013/03/13 - 2013/03/21)
1. 2013/03: `Docker` released as `open-source`, used `LXC(LinuX Containers)` as its default execution environment
1. 2014: `Docker` released version 0.9, replaced `LXC`with its own component, which was written in the `Go` programming language

>1. [Docker(software)](https://en.wikipedia.org/wiki/Docker_(software))
>1. [Lightning Talk - The future of Linux Containers](https://pyvideo.org/pycon-us-2013/the-future-of-linux-containers.html#youtube)
>1. [Introduction to Docker and Containers](https://us.pycon.org/2016/schedule/presentation/1800/)

## Resource

### Book

1. Docker从入门到实践
    1. :computer: <https://yeasy.gitbook.io/docker_practice/>
    1. :computer: <https://vuepress.mirror.docker-practice.com/>
    1. :computer: <https://github.com/yeasy/docker_practice/>

### Course

1. :star2:[Learn Docker - Tutorials, Courses, and Books](https://gitconnected.com/learn/docker)
1. :star2:[Learn DevOps basics with this free 2-hour Docker course](https://www.freecodecamp.org/news/docker-devops-course/)
    1. [Video](https://www.youtube.com/watch?v=fqMOX6JJhGo)
    1. [Practice Labs](https://kodekloud.com/p/docker-labs)

### Playground

1. :star2:**Play with Docker(PWD)**
    1. [Play with Docker - Hands-on Docker Tutorials for Developers](https://www.docker.com/play-with-docker)
    1. [Play with Docker Docker Playground](https://labs.play-with-docker.com/)
    1. [Play with Docker Classroom](https://training.play-with-docker.com/)
1. [Learn Docker & Containers using Interactive Browser-Based Scenarios](https://www.katacoda.com/courses/docker)
