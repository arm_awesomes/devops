---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Git Introduce

<!-- slide -->
# VCS: What, Why and Who

<!-- slide -->

![git-transport-v1](./00_Image/git-transport-v1.png)

<!-- slide -->
# Main Centralized VCS

1. **[CVS(Concurrent Versions System)](https://en.wikipedia.org/wiki/Concurrent_Versions_System)：** November 19, 1990
1. SVN(Subversion)
1. IBM Rational ClearCase
1. Microsoft VSS(Visual SourceSafe)

<!-- slide -->
# Main Distributed VCS

1. BitKeeper: commercial DVCS
1. Mercurial
1. Bazaar
1. Git

<!-- slide -->
# Centralized vs Distributed VCS

<!-- slide -->
# Other Information

1. [Comparison of version-control software](https://en.wikipedia.org/wiki/Comparison_of_version-control_software)

<!-- slide -->
# What is Git

git: an unpleasant or contemptible person :question: :joy:

>[Git (/ɡɪt/)](https://en.wikipedia.org/wiki/Git) is a **distributed version-control system** for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for **distributed, non-linear workflows**.
>
>Git was created by **Linus Torvalds** in 2005 for development of the Linux kernel, with other kernel developers contributing to its initial development. Its current maintainer since 2005 is **Junio Hamano**.

<!-- slide -->
# [A Short History of Git](https://git-scm.com/book/en/v2/Getting-Started-A-Short-History-of-Git)

>The Linux kernel is an open source software project of fairly large scope. For most of the lifetime of the Linux kernel maintenance ( **1991 - 2002** ), changes to the software were passed around as **patches and archived files**. In **2002**, the Linux kernel project began using a **proprietary DVCS called BitKeeper**.
>
>In **2005**, the relationship between the community that developed the Linux kernel and the commercial company that developed BitKeeper broke down, and the tool’s free-of-charge status was revoked. This prompted the Linux development community (and in particular Linus Torvalds, the creator of Linux) to develop their own tool based on some of **the lessons they learned while using BitKeeper**.

<!-- slide -->
# [The Goals of Git](https://git-scm.com/book/en/v2/Getting-Started-A-Short-History-of-Git)

1. Speed
1. Simple design
1. Strong support for **non-linear development (thousands of parallel branches)**
1. **Fully distributed**
1. Able to handle large projects like the Linux kernel efficiently (**speed and data size**)

<!-- slide -->
# Install Git

1. Linux
    1. **`RPM-based Distribution`：** `sudo dnf install git-all`
    1. **`Debian-based Distribution`:** `sudo apt install git-all`
1. MacOS: `brew install git`
1. Windows: download and install `git` or `GitHub Desktop`
1. Android: `Pocket Git`
1. iOS: `Working Copy`

## :point_right: GUI Clients: <https://git-scm.com/downloads/guis/>

<!-- slide -->
# Minimum Configuration

```bash{.line-numbers}
git config [--system/--global/--local] user.name "Your_Name"
git config [--system/--global/--local] user.email "email@example.com"
```

<!-- slide -->
# Git Logical Components

1. **`workspace/working tree`（工作区）:** `.`
1. **`index/stage/cached`（缓存区）:** `./.git/index`
1. **`local repository`（本地仓库）:**
    1. **`HEAD`:** pointer point to current branch
    1. **`branches`:**
    1. **`tags`:**
1. **`stash`（暂存区）:**
1. **`remote repository`（远程仓库）：**

<!-- slide -->
# Git Physical Components

1. **`local folder`:** Storage of `workspace`
1. **`local ".git" folder`:** Storage of `stash`, `index` and `local repository`
1. **`remote .git folder`:** Storage of `repository` in remote(server or other computer)

<!-- slide -->
# Git Protocols

1. **`local path`:**
1. **`file`:**
1. **`https`:**
1. **`SSH`:**

<!-- slide -->
# Notes of Before Using Git

1. Git only track file not folder()
1. Git only see difference of text file (to each character) not binary file
1. Git can know some of file `mv` but not all, unless use `git mv`

<!-- slide -->
# Dissatisfaction of Binary Files

1. VCS can only fully control text files, in other words, you can see differences of each commit
1. Git will incremental commit text files, binary files will be committed entire each time

<!-- slide -->
# Git Command Syntax

1. **`git <command> [options] [<subcommand>] [<argu> <values>]`**
1. **`-<short-argu> <values>|--<full-argu> <values>`**

<!-- slide -->
# Git Config

1. `local`: repository, `./.git/config`
1. `global`: all repositories in current user,  `~/.gitconfig`
1. `system`: all repositories in machine, `/etc/gitconfig`

```bash{.line-numbers}
# edit configuration with default editor, default is edit --local
git config -e [--local|--global|--system]
```

<!-- slide -->
# Thank You

## :ok:End of This Section:ok:
