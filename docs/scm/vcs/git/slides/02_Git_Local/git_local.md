---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Git Basic

<!-- slide -->
# Main Agendas

1. Git Logical and Physical Components
1. Initialize Repository

<!-- slide -->
# Git Use Recommend

<!-- slide -->
# `git init`

<!-- slide -->
# `git clone`

<!-- slide -->
# `git commit`

```bash{.line-numbers}
git commit -m"message"
git commit -am"message"
git commit --amend
```

<!-- slide -->
# `git status` - show track status

<!-- slide -->
# `git diff` - show changes of content

```bash{.line-numbers}
git diff ==> diff between workspace and stage(HEAD+1)
git diff <commit> ==> diff between workspace and <commit>
git diff --cache ==> diff between stage and HEAD
git diff --cache <commit> ==> diff between stage and <commit>
```

<!-- slide -->
# `git log` - Show Commit Histories

1. **`--petty=oneline`:**
1. **`--oneline`:** shorthand for "`--pretty=oneline --abbrev-commit`"
1. **`--graph`:**

<!-- slide -->
# `gitk` --> The git repository browser

<!-- slide -->
# `git-gui` - 

<!-- slide -->
# `git checkout`

1. `git checkout -- <file>`:  change file back to last `git add` or `git commit` status
1. `git checkout <branch>`: switch to a existed branch
1. `git checkout -b <branch>`: create and switch to a new branch, it equals to `git branch <branch>` and then `git checkout <branch>`

<!-- slide -->
# `git reset`

1. `git reset --hard`: reset `stage` and `workspace` to a `repository` status
1. `git reset [--soft]`: reset `stage` to a `repository` status

<!-- slide -->
# `git reflog` 

<!-- slide -->
# `git switch` --> New Command to `git checkout <branch>`

<!-- slide -->
# `git branch`

1. `git branch`: show branches
1. `git branch <branch>`: create a branch
1. `git branch -d <branch>`: delete a branch

<!-- slide -->
# `git add`

<!-- slide -->
# `git rm`

<!-- slide -->
# `git mv`

<!-- slide -->
# `git clean`

1. 

<!-- slide -->
# `git cherry-pick`

<!-- slide -->
# `git rebase`

1. `rebase` scenarios：
    1. combine multi-commit：

<!-- slide -->
# Fast Forward

1. **`git config merge.ff <true|false>`**
