---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Git Branch

<!-- slide -->
# `git branch`

```markdown{.line-numbers}
git branch
git branch -a
```

<!-- slide -->
# `git switch`

<!-- slide -->
# `git checkout`

<!-- slide -->
# `git merge`

```markdown{.line-numbers}
git merge <branch>
git merge --squash <branch>
```

<!-- slide -->
# Branch Limits

1. If branch b exists, no branch named b/anything can be created.

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
