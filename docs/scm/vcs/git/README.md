# Git

## 是什么What

## 为什么Why

1. 免费且开源free and open source：
1. 极速且易扩展super fast and scalable：
1. 低成本的分支与合并cheap branching/merging：社会化编程（social programming）的必要条件之一

## 怎样学How

核心关注点：

1. how to tracking history
1. how to working together

使用方法：

1. 命令行command line:
1. 图形界面graphical user interface：
1. 编辑器与IDE插件plugin of code editor & IDE：

## 学习资源Learning Resources

### 集锦Collections

1. :star2: [git official document collection](https://git-scm.com/doc)
1. :star2: [GitHub Learning Lab](https://lab.github.com/): include many techniques resources besides `Git`, `GitHub`, `Markdown`, `Node` and so on

### 入门级书籍教程Starter Books and Tutorials

1. :star2: [廖雪峰-Git教程](https://www.liaoxuefeng.com/wiki/896043488029600)
1. [git community book](http://shafiul.github.io/gitbook/index.html)
    1. [git community book中译本](http://gitbook.liuhui998.com/)
1. [Learn Git - Tutorials, Courses, and Books](https://gitconnected.com/learn/git)
1. [Atlassian-Bitbucket: Tutorials](https://www.atlassian.com/git/tutorials)
1. [Git Ready: learn git one commit at a time](http://gitready.com/)
1. [Learn Git Version Control using Interactive Browser-Based Scenarios](https://www.katacoda.com/courses/git)

### 入门级练习Starter Playgrounds

1. [GitHub: Resources to learn Git](http://try.github.io/)
    1. :star2: [Git-it: a (Mac, Win, Linux) Desktop App for Learning Git and GitHub](https://github.com/jlord/git-it-electron)
    1. :star2: [Learn Git branching](https://learngitbranching.js.org/)
    1. [Visualizing Git](http://git-school.github.io/visualizing-git/) : :point_right: this playground is lack of enough instructions
1. [Getting started with git](https://play.instruqt.com/public/topics/getting-started-with-git)

### 进阶级书籍Advanced Books

1. :star2: [Pro Git: An Open Source Book](https://git-scm.com/book/en/v2)

### 进阶级练习Advanced Playgrounds

1. :star2: [Git exercises](https://gitexercises.fracz.com)

### 专家级书籍Expect Books

### 专家级练习Expect Playgrounds

### Topic: Workflow

1. [Introduction to GitLab Flow (with Git Flow& GitHub Flow Compared)](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
1. [GitLab Workflow: An Overview](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/)
1. [Understanding the GitHub flow](https://guides.github.com/introduction/flow/)
1. [5 types of Git workflow that will help you deliver better code](https://buddy.works/blog/5-types-of-git-workflows)

<!--
1. https://github.com/git/git-scm.com/issues/1239
-->
