# Some Details

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [git merge vs git rebase](#git-merge-vs-git-rebase)
2. [git push --force vs git push --force-with-lease](#git-push-force-vs-git-push-force-with-lease)

<!-- /code_chunk_output -->

## git merge vs git rebase

## git push --force vs git push --force-with-lease
