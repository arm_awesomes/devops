# Issue

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [What is Issue](#what-is-issue)
2. [CASE Project Management Tools](#case-project-management-tools)
    1. [Microsoft Project](#microsoft-project)
    2. [Tencent TAPD](#tencent-tapd)
3. [Bibliography](#bibliography)
4. [Furthermore](#furthermore)

<!-- /code_chunk_output -->

## What is Issue

## CASE Project Management Tools

### Microsoft Project

### Tencent TAPD

>homepage: <https://www.tapd.cn/>

## Bibliography

1. 如何使用 Issue 管理软件项目？: <http://www.ruanyifeng.com/blog/2017/08/issue.html>
1. <https://robinpowered.com/blog/best-practice-system-for-organizing-and-tagging-github-issues/>

## Furthermore
