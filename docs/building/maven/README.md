# Maven

## Maven项目默认目录

```sh {.line-numbers}
${basedir}
|-- pom.xml
|-- src
|	|-- main
|	|	`-- java
|	|	`-- resources
|	|	`-- filters
|	`-- test
|	|	`-- java
|	|	`-- resources
|	|	`-- filters
|	`-- it
|	`-- assembly
|	`-- site
`-- LICENSE
`-- NOTICE
`-- README
```

## Maven仓库Maven Repositories

1. :star2: [MVN Repository](https://mvnrepository.com/)
1. :star2: [阿里云Maven中央仓库](https://maven.aliyun.com/mvn/guide)
