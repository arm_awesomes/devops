# DevOps

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [是什么What](#是什么what)
2. [为什么Why](#为什么why)

<!-- /code_chunk_output -->

## 是什么What

- 软件工程角度： 一组过程、方法与系统的统称
- 企业流程角度： 用于促进开发、运维和质量保障部门之间的沟通、协作和整合

![devops_tools_in_sdlc](./resources/image/devops_tools_in_sdlc.jpg)

1. 需求/规划：
    1. 项目管理
    1. 管理看板
1. 开发：
    1. 代码托管
    1. 代码检查
    1. 编译构建
    1. 依赖关系
1. 测试：
    1. 测试管理
    1. 功能测试
    1. 接口测试
    1. 非功能测试
1. 发布：
    1. 部署
    1. 发布

## 为什么Why
